#include "test_data.h"
#include "time.h"

void insert_first_test_data(database* db) {
    query* q = query_init_blank();
    q->type = INSERT;
    q->db = db;
    q->first_table_name = "People";

    data* dt = init_data(4);

    enum result res = init_str_col_data(dt, 0, "Name", "Roman");
    if (res != OK) {
        printf("In init()\n");
        printf("%s", result_strings[res]);
        close_query(q);
        close_data(dt);
        db_close(db);
        exit(EXIT_FAILURE);
    }
    res = init_int32_col_data(dt, 1, "Age", 20);
    if (res != OK) {
        printf("In init()\n");
        printf("%s", result_strings[res]);
        close_query(q);
        close_data(dt);
        db_close(db);
        exit(EXIT_FAILURE);
    }
    res = init_double_col_data(dt, 2, "Height (m)", 1.65);
    if (res != OK) {
        printf("In init()\n");
        printf("%s", result_strings[res]);
        close_query(q);
        close_data(dt);
        db_close(db);
        exit(EXIT_FAILURE);
    }
    res = init_bool_col_data(dt, 3, "Is Man", true);
    if (res != OK) {
        printf("In init()\n");
        printf("%s", result_strings[res]);
        close_query(q);
        close_data(dt);
        db_close(db);
        exit(EXIT_FAILURE);
    }

    q->dt = dt;

    res = execute_query(q, NULL);
    if (res != OK) {
        printf("In execute_query()\n");
        printf("%s", result_strings[res]);
        close_query(q);
        close_data(dt);
        db_close(db);
        exit(EXIT_FAILURE);
    }

    close_data(dt);

    dt = init_data(4);

    res = init_str_col_data(dt, 0, "Name", "Victoria");
    if (res != OK) {
        printf("In init()\n");
        printf("%s", result_strings[res]);
        close_query(q);
        close_data(dt);
        db_close(db);
        exit(EXIT_FAILURE);
    }
    res = init_int32_col_data(dt, 1, "Age", 20);
    if (res != OK) {
        printf("In init()\n");
        printf("%s", result_strings[res]);
        close_query(q);
        close_data(dt);
        db_close(db);
        exit(EXIT_FAILURE);
    }
    res = init_double_col_data(dt, 2, "Height (m)", 1.65);
    if (res != OK) {
        printf("In init()\n");
        printf("%s", result_strings[res]);
        close_query(q);
        close_data(dt);
        db_close(db);
        exit(EXIT_FAILURE);
    }
    res = init_bool_col_data(dt, 3, "Is Man", false);
    if (res != OK) {
        printf("In init()\n");
        printf("%s", result_strings[res]);
        close_query(q);
        close_data(dt);
        db_close(db);
        exit(EXIT_FAILURE);
    }

    q->dt = dt;
    res = execute_query(q, NULL);
    if (res != OK) {
        printf("In execute_query()\n");
        printf("%s", result_strings[res]);
        close_query(q);
        close_data(dt);
        db_close(db);
        exit(EXIT_FAILURE);
    }

    close_data(dt);

    dt = init_data(4);

    res = init_str_col_data(dt, 0, "Name", "Anastasia");
    if (res != OK) {
        printf("In init()\n");
        printf("%s", result_strings[res]);
        close_query(q);
        close_data(dt);
        db_close(db);
        exit(EXIT_FAILURE);
    }
    res = init_int32_col_data(dt, 1, "Age", 30);
    if (res != OK) {
        printf("In init()\n");
        printf("%s", result_strings[res]);
        close_query(q);
        close_data(dt);
        db_close(db);
        exit(EXIT_FAILURE);
    }
    res = init_double_col_data(dt, 2, "Height (m)", 1.72);
    if (res != OK) {
        printf("In init()\n");
        printf("%s", result_strings[res]);
        close_query(q);
        close_data(dt);
        db_close(db);
        exit(EXIT_FAILURE);
    }
    res = init_bool_col_data(dt, 3, "Is Man", false);
    if (res != OK) {
        printf("In init()\n");
        printf("%s", result_strings[res]);
        close_query(q);
        close_data(dt);
        db_close(db);
        exit(EXIT_FAILURE);
    }

    q->dt = dt;
    res = execute_query(q, NULL);
    if (res != OK) {
        printf("In execute_query()\n");
        printf("%s", result_strings[res]);
        close_query(q);
        close_data(dt);
        db_close(db);
        exit(EXIT_FAILURE);
    }

    close_data(dt);

    dt = init_data(4);

    res = init_str_col_data(dt, 0, "Name", "Mikhail");
    if (res != OK) {
        printf("In init()\n");
        printf("%s", result_strings[res]);
        close_query(q);
        close_data(dt);
        db_close(db);
        exit(EXIT_FAILURE);
    }
    res = init_int32_col_data(dt, 1, "Age", 20);
    if (res != OK) {
        printf("In init()\n");
        printf("%s", result_strings[res]);
        close_query(q);
        close_data(dt);
        db_close(db);
        exit(EXIT_FAILURE);
    }
    res = init_double_col_data(dt, 2, "Height (m)", 1.85);
    if (res != OK) {
        printf("In init()\n");
        printf("%s", result_strings[res]);
        close_query(q);
        close_data(dt);
        db_close(db);
        exit(EXIT_FAILURE);
    }
    res = init_bool_col_data(dt, 3, "Is Man", true);
    if (res != OK) {
        printf("In init()\n");
        printf("%s", result_strings[res]);
        close_query(q);
        close_data(dt);
        db_close(db);
        exit(EXIT_FAILURE);
    }

    q->dt = dt;
    res = execute_query(q, NULL);
    if (res != OK) {
        printf("In execute_query()\n");
        printf("%s", result_strings[res]);
        close_query(q);
        close_data(dt);
        db_close(db);
        exit(EXIT_FAILURE);
    }

    close_data(dt);

    dt = init_data(4);

    res = init_str_col_data(dt, 0, "Name", "Maria");
    if (res != OK) {
        printf("In init()\n");
        printf("%s", result_strings[res]);
        close_query(q);
        close_data(dt);
        db_close(db);
        exit(EXIT_FAILURE);
    }
    res = init_int32_col_data(dt, 1, "Age", 20);
    if (res != OK) {
        printf("In init()\n");
        printf("%s", result_strings[res]);
        close_query(q);
        close_data(dt);
        db_close(db);
        exit(EXIT_FAILURE);
    }
    res = init_double_col_data(dt, 2, "Height (m)", 1.70);
    if (res != OK) {
        printf("In init()\n");
        printf("%s", result_strings[res]);
        close_query(q);
        close_data(dt);
        db_close(db);
        exit(EXIT_FAILURE);
    }
    res = init_bool_col_data(dt, 3, "Is Man", false);
    if (res != OK) {
        printf("In init()\n");
        printf("%s", result_strings[res]);
        close_query(q);
        close_data(dt);
        db_close(db);
        exit(EXIT_FAILURE);
    }

    q->dt = dt;
    res = execute_query(q, NULL);
    if (res != OK) {
        printf("In execute_query()\n");
        printf("%s", result_strings[res]);
        close_query(q);
        close_data(dt);
        db_close(db);
        exit(EXIT_FAILURE);
    }

    close_data(dt);

    dt = init_data(4);

    res = init_str_col_data(dt, 0, "Name", "Farid");
    if (res != OK) {
        printf("In init()\n");
        printf("%s", result_strings[res]);
        close_query(q);
        close_data(dt);
        db_close(db);
        exit(EXIT_FAILURE);
    }
    res = init_int32_col_data(dt, 1, "Age", 19);
    if (res != OK) {
        printf("In init()\n");
        printf("%s", result_strings[res]);
        close_query(q);
        close_data(dt);
        db_close(db);
        exit(EXIT_FAILURE);
    }
    res = init_double_col_data(dt, 2, "Height (m)", 1.83);
    if (res != OK) {
        printf("In init()\n");
        printf("%s", result_strings[res]);
        close_query(q);
        close_data(dt);
        db_close(db);
        exit(EXIT_FAILURE);
    }
    res = init_bool_col_data(dt, 3, "Is Man", true);
    if (res != OK) {
        printf("In init()\n");
        printf("%s", result_strings[res]);
        close_query(q);
        close_data(dt);
        db_close(db);
        exit(EXIT_FAILURE);
    }

    q->dt = dt;
    res = execute_query(q, NULL);
    if (res != OK) {
        printf("In execute_query()\n");
        printf("%s", result_strings[res]);
        close_query(q);
        close_data(dt);
        db_close(db);
        exit(EXIT_FAILURE);
    }

    close_data(dt);
    close_query(q);
}

void insert_second_test_data(database* db) {
    query* q = query_init_blank();
    q->type = INSERT;
    q->db = db;
    q->first_table_name = "Passports";
    enum result res;

    data* dt = init_data(2);

    res = init_str_col_data(dt, 0, "Name", "Roman");
    if (res != OK) {
        printf("In init()\n");
        printf("%s", result_strings[res]);
        close_query(q);
        close_data(dt);
        db_close(db);
        exit(EXIT_FAILURE);
    }
    res = init_str_col_data(dt, 1, "Passport ID", "36 16 420228");
    if (res != OK) {
        printf("In init()\n");
        printf("%s", result_strings[res]);
        close_query(q);
        close_data(dt);
        db_close(db);
        exit(EXIT_FAILURE);
    }

    q->dt = dt;
    res = execute_query(q, NULL);
    if (res != OK) {
        printf("In execute_query()\n");
        printf("%s", result_strings[res]);
        close_query(q);
        close_data(dt);
        db_close(db);
        exit(EXIT_FAILURE);
    }

    close_data(dt);

    dt = init_data(2);

    res = init_str_col_data(dt, 0, "Name", "Victoria");
    if (res != OK) {
        printf("In init()\n");
        printf("%s", result_strings[res]);
        close_query(q);
        close_data(dt);
        db_close(db);
        exit(EXIT_FAILURE);
    }
    res = init_str_col_data(dt, 1, "Passport ID", "36 16 322228");
    if (res != OK) {
        printf("In init()\n");
        printf("%s", result_strings[res]);
        close_query(q);
        close_data(dt);
        db_close(db);
        exit(EXIT_FAILURE);
    }

    q->dt = dt;
    res = execute_query(q, NULL);
    if (res != OK) {
        printf("In execute_query()\n");
        printf("%s", result_strings[res]);
        close_query(q);
        close_data(dt);
        db_close(db);
        exit(EXIT_FAILURE);
    }

    close_data(dt);

    dt = init_data(2);

    res = init_str_col_data(dt, 0, "Name", "Anastasia");
    if (res != OK) {
        printf("In init()\n");
        printf("%s", result_strings[res]);
        close_query(q);
        close_data(dt);
        db_close(db);
        exit(EXIT_FAILURE);
    }
    res = init_str_col_data(dt, 1, "Passport ID", "36 16 322420");
    if (res != OK) {
        printf("In init()\n");
        printf("%s", result_strings[res]);
        close_query(q);
        close_data(dt);
        db_close(db);
        exit(EXIT_FAILURE);
    }

    q->dt = dt;
    res = execute_query(q, NULL);
    if (res != OK) {
        printf("In execute_query()\n");
        printf("%s", result_strings[res]);
        close_query(q);
        close_data(dt);
        db_close(db);
        exit(EXIT_FAILURE);
    }

    close_data(dt);

    dt = init_data(2);

    res = init_str_col_data(dt, 0, "Name", "Mikhail");
    if (res != OK) {
        printf("In init()\n");
        printf("%s", result_strings[res]);
        close_query(q);
        close_data(dt);
        db_close(db);
        exit(EXIT_FAILURE);
    }
    res = init_str_col_data(dt, 1, "Passport ID", "36 16 228322");
    if (res != OK) {
        printf("In init()\n");
        printf("%s", result_strings[res]);
        close_query(q);
        close_data(dt);
        db_close(db);
        exit(EXIT_FAILURE);
    }

    q->dt = dt;
    res = execute_query(q, NULL);
    if (res != OK) {
        printf("In execute_query()\n");
        printf("%s", result_strings[res]);
        close_query(q);
        close_data(dt);
        db_close(db);
        exit(EXIT_FAILURE);
    }

    close_data(dt);

    dt = init_data(2);

    res = init_str_col_data(dt, 0, "Name", "Maria");
    if (res != OK) {
        printf("In init()\n");
        printf("%s", result_strings[res]);
        close_query(q);
        close_data(dt);
        db_close(db);
        exit(EXIT_FAILURE);
    }
    res = init_str_col_data(dt, 1, "Passport ID", "36 16 228420");
    if (res != OK) {
        printf("In init()\n");
        printf("%s", result_strings[res]);
        close_query(q);
        close_data(dt);
        db_close(db);
        exit(EXIT_FAILURE);
    }

    q->dt = dt;
    res = execute_query(q, NULL);
    if (res != OK) {
        printf("In execute_query()\n");
        printf("%s", result_strings[res]);
        close_query(q);
        close_data(dt);
        db_close(db);
        exit(EXIT_FAILURE);
    }

    close_data(dt);

    dt = init_data(2);

    res = init_str_col_data(dt, 0, "Name", "Farid");
    if (res != OK) {
        printf("In init()\n");
        printf("%s", result_strings[res]);
        close_query(q);
        close_data(dt);
        db_close(db);
        exit(EXIT_FAILURE);
    }
    res = init_str_col_data(dt, 1, "Passport ID", "36 16 148888");
    if (res != OK) {
        printf("In init()\n");
        printf("%s", result_strings[res]);
        close_query(q);
        close_data(dt);
        db_close(db);
        exit(EXIT_FAILURE);
    }

    q->dt = dt;
    res = execute_query(q, NULL);
    if (res != OK) {
        printf("In execute_query()\n");
        printf("%s", result_strings[res]);
        close_query(q);
        close_data(dt);
        db_close(db);
        exit(EXIT_FAILURE);
    }

    close_data(dt);
    close_query(q);
}

void test_large_data_performance(database* db) {
    query* insert = query_init_blank();
    insert->type = CREATE_TABLE;
    insert->db = db;
    insert->first_table_name = "Large_Test_Table";

    data* dt = init_data(1);
    enum result res = init_int32_col_data(dt, 0, "num", 0);
    if (res != OK) {
        printf("In init_int32_col_data()\n");
        printf("%s", result_strings[res]);
        close_query(insert);
        close_data(dt);
        db_close(db);
        exit(EXIT_FAILURE);
    }

    insert->dt = dt;

    res = execute_query(insert, NULL);
    if (res != OK) {
        printf("In execute_query()\n");
        printf("%s", result_strings[res]);
        close_query(insert);
        close_data(dt);
        db_close(db);
        exit(EXIT_FAILURE);
    }

    insert->type = INSERT;

    clock_t start, end;
    double cpu_time_used;
    row_iterator* iterator;

    query* select = query_init_blank();
    select->type = SELECT;
    select->first_table_name = "Large_Test_Table";
    select->col_type = INT32;
    int32_t val = 2;
    select->value = &val;
    select->col_name_first = "num";
    select->db = db;

    query* delete = query_init_blank();
    delete->type = DELETE;
    delete->first_table_name = "Large_Test_Table";
    delete->col_type = INT32;
    int32_t r_val = 1;
    delete->value = &r_val;
    delete->col_name_first = "num";
    delete->db = db;

    close_data(dt);

    off_t size = lseek(get_fd(db), 0, SEEK_END);
    printf("Size of file now: %ld\n", size);

    for (int k = 0; k < 20; k++) {
        start = clock();
        for (int i = 0; i < 10000; i++) {
            dt = init_data(1);
            res = init_int32_col_data(dt, 0, "num", i%3);
            if (res != OK) {
                printf("In init_int32_col_data()\n");
                printf("%s", result_strings[res]);
                close_query(insert);
                close_query(select);
                close_data(dt);
                db_close(db);
                exit(EXIT_FAILURE);
            }
            insert->dt = dt;
            res = execute_query(insert, NULL);
            if (res != OK) {
                printf("In execute_query()\n");
                printf("%s", result_strings[res]);
                close_query(insert);
                close_query(select);
                close_data(dt);
                db_close(db);
                exit(EXIT_FAILURE);
            }
            if (i % 100 == 0) {
                printf(".");
                fflush(stdout);
            }
            close_data(dt);
        }
        end = clock();
        cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;
        printf("\nInsert 20k :%f\n", cpu_time_used);
        start = clock();
        iterator = iter_init();
        res = execute_query(select, iterator);
        if (res != OK) {
            printf("In execute_query()\n");
            printf("%s", result_strings[res]);
            close_query(insert);
            close_query(select);
            close_data(dt);
            db_close(db);
            exit(EXIT_FAILURE);
        }
        close_iterator(iterator, db);
        end = clock();
        cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;
        printf("Select where num = %d :%f\n", val, cpu_time_used);

        size = lseek(get_fd(db), 0, SEEK_END);
        printf("Size of file now: %ld\n", size);
        start = clock();
        res = execute_query(delete, NULL);
        if (res != OK) {
            printf("In execute_query()\n");
            printf("%s", result_strings[res]);
            close_query(insert);
            close_query(select);
            close_query(delete);
            close_data(dt);
            db_close(db);
            exit(EXIT_FAILURE);
        }

        end = clock();
        cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;
        printf("Remove where num = %d :%f\n", r_val, cpu_time_used);
        size = lseek(get_fd(db), 0, SEEK_END);
        printf("Size of file now: %ld\n", size);
    }
    printf("\n");


    query* drop = query_init_blank();
    drop->type = DROP_TABLE;
    drop->first_table_name = "Large_Test_Table";
    drop->db = db;

    res = execute_query(drop, NULL);
    if (res != OK) {
        printf("In execute_query()\n");
        printf("%s", result_strings[res]);
        close_query(insert);
        close_query(select);
        close_query(delete);
        close_query(drop);
        close_data(dt);
        db_close(db);
        exit(EXIT_FAILURE);
    }

    size = lseek(get_fd(db), 0, SEEK_END);
    printf("Size of file after dropping table: %ld\n", size);

    dt = init_data(1);
    res = init_int32_col_data(dt, 0, "num", 0);
    if (res != OK) {
        printf("In init_int32_col_data()\n");
        printf("%s", result_strings[res]);
        close_query(insert);
        close_query(delete);
        close_query(select);
        close_query(drop);
        close_data(dt);
        db_close(db);
        exit(EXIT_FAILURE);
    }

    insert->type = CREATE_TABLE;
    insert->dt = dt;

    res = execute_query(insert, NULL);
    if (res != OK) {
        printf("In execute_query()\n");
        printf("%s", result_strings[res]);
        close_query(insert);
        close_query(select);
        close_query(delete);
        close_query(drop);
        close_data(dt);
        db_close(db);
        exit(EXIT_FAILURE);
    }

    insert->type = INSERT;

    close_data(dt);

    for (int k = 0; k < 20; k++) {
        start = clock();
        for (int i = 0; i < 10000; i++) {
            dt = init_data(1);
            res = init_int32_col_data(dt, 0, "num", i%3 + 1);
            if (res != OK) {
                printf("In init_int32_col_data()\n");
                printf("%s", result_strings[res]);
                close_query(insert);
                close_query(select);
                close_query(delete);
                close_query(drop);
                close_data(dt);
                db_close(db);
                exit(EXIT_FAILURE);
            }
            insert->dt = dt;
            res = execute_query(insert, NULL);
            if (res != OK) {
                printf("In execute_query()\n");
                printf("%s", result_strings[res]);
                close_query(insert);
                close_query(select);
                close_query(delete);
                close_query(drop);
                close_data(dt);
                db_close(db);
                exit(EXIT_FAILURE);
            }
            if (i % 100 == 0) {
                printf(".");
                fflush(stdout);
            }
            close_data(dt);
        }
        end = clock();
        cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;
        printf("\nInsert 20k :%f\n", cpu_time_used);
        size = lseek(get_fd(db), 0, SEEK_END);
        printf("Size of file now: %ld\n", size);
    }
    printf("\n");


    close_query(drop);
    close_query(delete);
    close_query(insert);
    close_query(select);
}

void test_delete(database* db) {
    query* insert = query_init_blank();
    insert->type = CREATE_TABLE;
    insert->db = db;
    insert->first_table_name = "Large_Test_Table";

    data* dt = init_data(1);
    enum result res = init_int32_col_data(dt, 0, "num", 0);
    if (res != OK) {
        printf("In init_int32_col_data()\n");
        printf("%s", result_strings[res]);
        close_query(insert);
        close_data(dt);
        db_close(db);
        exit(EXIT_FAILURE);
    }

    insert->dt = dt;
    res = execute_query(insert, NULL);
    if (res != OK) {
        printf("In execute_query()\n");
        printf("%s", result_strings[res]);
        close_query(insert);
        close_data(dt);
        db_close(db);
        exit(EXIT_FAILURE);
    }

    insert->type = INSERT;
    off_t size = lseek(get_fd(db), 0, SEEK_END);
    for (int i = 0; i < 20; i++) {
        printf("Size of file now: %ld\n", size);
        for (int j = 0; j < 10000; j++) {
            execute_query(insert, NULL);
        }
        size = lseek(get_fd(db), 0, SEEK_END);
    }
    printf("Size of file now: %ld\n", size);
    printf("==========================================\n");
    query* delete = query_init_blank();
    delete->type = DELETE;
    delete->first_table_name = "Large_Test_Table";
    delete->col_type = INT32;
    int32_t r_val = 0;
    delete->value = &r_val;
    delete->col_name_first = "num";
    delete->db = db;
    execute_query(delete, NULL);
    size = lseek(get_fd(db), 0, SEEK_END);
    for (int i = 0; i < 20; i++) {
        printf("Size of file now: %ld\n", size);
        for (int j = 0; j < 10000; j++) {
            execute_query(insert, NULL);
        }
        size = lseek(get_fd(db), 0, SEEK_END);
    }
    printf("Size of file now: %ld\n", size);

    close_query(insert);
    close_query(delete);
    close_data(dt);
}

void test_join(database* db) {
    query* create = query_init_blank();
    create->type = CREATE_TABLE;
    create->db = db;
    create->first_table_name = "Join_Test_Table_1";

    data* first_data = init_data(2);
    init_int32_col_data(first_data, 0, "num", 0);
    init_bool_col_data(first_data, 1, "bool", true);

    create->dt = first_data;
    execute_query(create, NULL);

    close_data(first_data);

    create->first_table_name = "Join_Test_Table_2";

    data* second_data = init_data(2);
    init_int32_col_data(second_data, 0, "num", 0);
    init_double_col_data(second_data, 1, "double", 1);

    create->dt = second_data;
    execute_query(create, NULL);

    close_data(second_data);
    close_query(create);
    srand(time(NULL));

    query* insert = query_init_blank();
    insert->type = INSERT;
    insert->db = db;

    clock_t start, end;
    double cpu_time_used;

    query* join = query_init_blank();
    join->type = JOIN;
    join->first_table_name = "Join_Test_Table_1";
    join->second_table_name = "Join_Test_Table_2";
    join->db = db;
    join->col_name_first = "num";
    join->col_name_second = "num";
    row_iterator* iterator;

    for (int i = 0; i < 20; i++) {
        insert->first_table_name = "Join_Test_Table_1";
        for (int j = 0; j < 100; j++) {
            first_data = init_data(2);
            init_int32_col_data(first_data, 0, "num", rand() % 100 - 50);
            init_bool_col_data(first_data, 1, "bool", true);

            insert->dt = first_data;
            execute_query(insert, NULL);
            close_data(first_data);
        }
        insert->first_table_name = "Join_Test_Table_2";
        for (int j = 0; j < 100; j++) {
            second_data = init_data(2);
            init_int32_col_data(second_data, 0, "num", rand() % 100);
            init_double_col_data(second_data, 1, "double", 1);

            insert->dt = second_data;
            execute_query(insert, NULL);
            close_data(second_data);
        }
        iterator = iter_init();
        start = clock();

        execute_query(join, iterator);

        end = clock();
        close_iterator(iterator, db);
        cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;
        printf("Join time: %f\n", cpu_time_used);
    }


    close_query(insert);
    close_query(join);
}