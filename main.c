#include "query.h"
#include "test_data.h"


int main() {
    // Open your database:
    database* db = db_init();
    enum result res = db_open(db, "my_db.db", 1, 100);
    if (res != OK) {
        printf("In db_open()\n");
        printf("%s", result_strings[res]);
        free(db);
        return 1;
    }
    data* dt;
    row_iterator* iter;

    // Create table:
    query* q = query_init_blank();
    q->type = CREATE_TABLE;
    q->db = db;
    q->first_table_name = "People";

    // To specify columns in table we use data object as pattern:
    dt = init_data(4);

    // Init columns
    res = init_str_col_data(dt, 0, "Name", "Vladislav");
    if (res != OK) {
        printf("In init_str_col_data()\n");
        printf("%s", result_strings[res]);
        close_data(dt);
        close_query(q);
        db_close(db);
        return 1;
    }
    res = init_int32_col_data(dt, 1, "Age", 20);
    if (res != OK) {
        printf("In init_int32_col_data()\n");
        printf("%s", result_strings[res]);
        close_data(dt);
        close_query(q);
        db_close(db);
        return 1;
    }
    res = init_double_col_data(dt, 2, "Height (m)", 1.87);
    if (res != OK) {
        printf("In init_double_col_data()\n");
        printf("%s", result_strings[res]);
        close_data(dt);
        close_query(q);
        db_close(db);
        return 1;
    }
    res = init_bool_col_data(dt, 3, "Is Man", true);
    if (res != OK) {
        printf("In init_bool_col_data()\n");
        printf("%s", result_strings[res]);
        close_data(dt);
        close_query(q);
        db_close(db);
        return 1;
    }

    q->dt = dt;

    res = execute_query(q, NULL);
    if (res != OK) {
        printf("In execute_query()\n");
        printf("%s", result_strings[res]);
        close_query(q);
        close_data(dt);
        db_close(db);
        return 1;
    }

    // Don't forget to close query and data after you used them!
    close_query(q);
    close_data(dt);

    // Init Data "object":
    dt = init_data(4);
    // Init data:
    res = init_str_col_data(dt, 0, "Name", "Vladislav");
    if (res != OK) {
        printf("In init_str_col_data()\n");
        printf("%s", result_strings[res]);
        close_data(dt);
        db_close(db);
        return 1;
    }
    res = init_int32_col_data(dt, 1, "Age", 20);
    if (res != OK) {
        printf("In init_int32_col_data()\n");
        printf("%s", result_strings[res]);
        close_data(dt);
        db_close(db);
        return 1;
    }
    res = init_double_col_data(dt, 2, "Height (m)", 1.87);
    if (res != OK) {
        printf("In init_double_col_data()\n");
        printf("%s", result_strings[res]);
        close_data(dt);
        db_close(db);
        return 1;
    }
    res = init_bool_col_data(dt, 3, "Is Man", true);
    if (res != OK) {
        printf("In init_bool_col_data()\n");
        printf("%s", result_strings[res]);
        close_data(dt);
        db_close(db);
        return 1;
    }

    // Insert data into table:
    q = query_init_blank();
    q->type = INSERT;
    q->db = db;
    q->first_table_name = "People";
    q->dt = dt;

    res = execute_query(q, NULL);
    if (res != OK) {
        printf("In execute_query()\n");
        printf("%s", result_strings[res]);
        close_query(q);
        close_data(dt);
        db_close(db);
        return 1;
    }


    close_query(q);
    close_data(dt);

    // Insert some more data:
    insert_first_test_data(db);

    // Select some values:
    int32_t val = 20;
    q = query_init_blank();
    q->type = SELECT;
    q->db = db;
    q->first_table_name = "People";
    q->col_name_first = "Age";
    q->col_type = INT32;
    q->value = &val;
    iter = iter_init();
    res = execute_query(q, iter);
    if (res != OK) {
        printf("In execute_query()\n");
        printf("%s", result_strings[res]);
        free(iter);
        close_query(q);
        close_data(dt);
        db_close(db);
        return 1;
    }
    close_query(q);
    // Let's print results:
    int counter = 0;
    printf("=========================================\n");
    while (iter_valid(iter)) {
        dt = extract_iterator(iter);
        for (int i = 0; i < dt->col_num; i++) {
            switch (dt->columns[i].header->col_header) {
                case INT32:
                    printf(" %d ", *((int32_t *) dt->data[i]));
                    break;
                case VARCHAR:
                    printf(" %s ", (char *) dt->data[i]);
                    break;
                case DOUBLE:
                    printf(" %.2f ", *((double *) dt->data[i]));
                    break;
                case BOOLEAN:
                    printf(" %s ", *((bool *) dt->data[i]) ? "true" : "false");
                    break;
            }
        }
        close_data(dt);
        printf("\n");
        res = table_next(iter);
        if (res != OK) {
            printf("In table_next()\n");
            printf("%s", result_strings[res]);
            close_iterator(iter, db);
            db_close(db);
            return 1;
        }
        counter++;
    }
    printf("=========================================\n");
    if (counter != 5) {
        printf("Something went wrong! Should've been selected 5 items, but got: %d", counter);
        close_iterator(iter, db);
        db_close(db);
        return 1;
    }

    // Don't forget to close Iterator after you finished it!
    close_iterator(iter, db);

    // Let's make a new table and fill it:
    q = query_init_blank();
    q->type = CREATE_TABLE;
    q->db = db;
    q->first_table_name = "Passports";

    dt = init_data(2);

    res = init_str_col_data(dt, 0, "Name", "Vladislav");
    if (res != OK) {
        printf("In init_str_col_data()\n");
        printf("%s", result_strings[res]);
        close_query(q);
        close_data(dt);
        db_close(db);
        return 1;
    }
    res = init_str_col_data(dt, 1, "Passport ID", "36 16 420322");
    if (res != OK) {
        printf("In init_str_col_data()\n");
        printf("%s", result_strings[res]);
        close_query(q);
        close_data(dt);
        db_close(db);
        return 1;
    }

    q->dt = dt;


    res = execute_query(q, NULL);
    if (res != OK) {
        printf("In execute_query()\n");
        printf("%s", result_strings[res]);
        close_query(q);
        close_data(dt);
        db_close(db);
        return 1;
    }

    close_query(q);

    q = query_init_blank();
    q->type = INSERT;
    q->db = db;
    q->dt = dt;
    q->first_table_name = "Passports";

    res = execute_query(q, NULL);
    if (res != OK) {
        printf("In execute_query()\n");
        printf("%s", result_strings[res]);
        close_query(q);
        close_data(dt);
        db_close(db);
        return 1;
    }

    close_query(q);
    close_data(dt);

    // Some more test data:
    insert_second_test_data(db);

    // Try updating:
    q = query_init_blank();
    q->type = UPDATE;
    q->db = db;
    q->first_table_name = "Passports";
    q->col_name_first = "Name";
    q->col_type = VARCHAR;
    q->value = "Roman";

    dt = init_data(2);
    res = init_str_col_data(dt, 0, "Name", "Vladislav");
    if (res != OK) {
        printf("In init_str_col_data()\n");
        printf("%s", result_strings[res]);
        close_query(q);
        close_data(dt);
        db_close(db);
        return 1;
    }
    res = init_str_col_data(dt, 1, "Passport ID", "36 16 420322");
    if (res != OK) {
        printf("In init_str_col_data()\n");
        printf("%s", result_strings[res]);
        close_query(q);
        close_data(dt);
        db_close(db);
        return 1;
    }

    q->dt = dt;

    res = execute_query(q, NULL);
    if (res != OK) {
        printf("In execute_query()\n");
        printf("%s", result_strings[res]);
        close_query(q);
        close_data(dt);
        db_close(db);
        return 1;
    }

    close_query(q);
    close_data(dt);

    // Check results:
    q = query_init_blank();
    q->type = SELECT;
    q->db = db;
    q->first_table_name = "Passports";
    q->col_name_first = "Name";
    q->col_type = VARCHAR;
    q->value = "Vladislav";
    iter = iter_init();
    res = execute_query(q, iter);
    if (res != OK) {
        printf("In execute_query()\n");
        printf("%s", result_strings[res]);
        free(iter);
        close_query(q);
        close_data(dt);
        db_close(db);
        return 1;
    }
    close_query(q);
    counter = 0;
    while (iter_valid(iter)) {
        dt = extract_iterator(iter);
        bool f = false;
        for (int i = 0; i < dt->col_num; i++) {
            if (str_equals(dt->columns[i].name, "Name") && dt->columns[i].header->col_header == VARCHAR) {
                f = true;
                if (!str_equals((char *)dt->data[i], "Vladislav")) {
                    printf("Something went wrong! Should've been selected Vladislav's, but got: %s", (char *) dt->data[i]);
                    close_data(dt);
                    close_iterator(iter, db);
                    db_close(db);
                    return 1;
                }
            }
        }
        if (!f) {
            printf("Something went wrong! No column found");
            close_data(dt);
            close_iterator(iter, db);
            db_close(db);
            return 1;
        }
        close_data(dt);
        counter++;
        res = table_next(iter);
        if (res != OK) {
            printf("In table_next()\n");
            printf("%s", result_strings[res]);
            close_iterator(iter, db);
            db_close(db);
            return 1;
        }
    }
    if (counter != 2) {
        printf("Something went wrong! Should've been selected 2 items, but got: %d", counter);
        close_iterator(iter, db);
        db_close(db);
        return 1;
    }

    close_iterator(iter, db);

    // Try removing:
    q = query_init_blank();
    q->type = DELETE;
    q->db = db;
    q->first_table_name = "Passports";
    q->col_name_first = "Name";
    q->col_type = VARCHAR;
    q->value = "Vladislav";

    res = execute_query(q, NULL);
    if (res != OK) {
        printf("In execute_query()\n");
        printf("%s", result_strings[res]);
        close_query(q);
        db_close(db);
        return 1;
    }

    close_query(q);

    // Check results:

    q = query_init_blank();
    q->type = SELECT_ALL;
    q->db = db;
    q->first_table_name = "Passports";
    iter = iter_init();
    res = execute_query(q, iter);
    if (res != OK) {
        printf("In execute_query()\n");
        printf("%s", result_strings[res]);
        free(iter);
        close_query(q);
        db_close(db);
        return 1;
    }
    close_query(q);
    counter = 0;
    while (iter_valid(iter)) {
        dt = extract_iterator(iter);
        bool f = false;
        for (int i = 0; i < dt->col_num; i++) {
            if (str_equals(dt->columns[i].name, "Name") && dt->columns[i].header->col_header == VARCHAR) {
                f = true;
                if (str_equals((char *)dt->data[i], "Vladislav")) {
                    printf("Something went wrong! Should've been deleted Vladislav's, but didn't");
                    close_data(dt);
                    close_iterator(iter, db);
                    db_close(db);
                    return 1;
                }
            }
        }
        if (!f) {
            printf("Something went wrong! No column found");
            close_data(dt);
            close_iterator(iter, db);
            db_close(db);
            return 1;
        }
        close_data(dt);
        counter++;
        res = table_next(iter);
        if (res != OK) {
            printf("In table_next()\n");
            printf("%s", result_strings[res]);
            close_iterator(iter, db);
            db_close(db);
            return 1;
        }
    }
    if (counter != 5) {
        printf("Something went wrong! Should've been selected 5 items, but got: %d", counter);
        close_iterator(iter, db);
        db_close(db);
        return 1;
    }

    close_iterator(iter, db);

    // Let's reopen database to check if we read from file correctly
    db_close(db);
    db = db_init();
    res = db_open(db, "my_db.db", 1, 100);
    if (res != OK) {
        printf("In db_open()\n");
        printf("%s", result_strings[res]);
        free(db);
        return 1;
    }
    // If we read correctly, next test will work correctly

    // Try joining tables:
    q = query_init_blank();
    q->type = JOIN;
    q->db = db;
    q->first_table_name = "Passports";
    q->col_name_first = "Name";
    q->second_table_name = "People";
    q->col_name_second = "Name";
    iter = iter_init();
    res = execute_query(q, iter);
    if (res != OK) {
        printf("In execute_query()\n");
        printf("%s", result_strings[res]);
        free(iter);
        close_query(q);
        db_close(db);
        return 1;
    }
    close_query(q);

    // Let's print results:
    counter = 0;
    printf("=========================================\n");
    while (iter_valid(iter)) {
        dt = extract_iterator(iter);
        for (int i = 0; i < dt->col_num; i++) {
            switch (dt->columns[i].header->col_header) {
                case INT32:
                    printf(" %d ", *((int32_t *) dt->data[i]));
                    break;
                case VARCHAR:
                    printf(" %s ", (char *) dt->data[i]);
                    break;
                case DOUBLE:
                    printf(" %.2f ", *((double *) dt->data[i]));
                    break;
                case BOOLEAN:
                    printf(" %s ", *((bool *) dt->data[i]) ? "true" : "false");
                    break;
            }
        }
        close_data(dt);
        printf("\n");
        res = table_next(iter);
        if (res != OK) {
            printf("In table_next()\n");
            printf("%s", result_strings[res]);
            close_iterator(iter, db);
            db_close(db);
            return 1;
        }
        counter++;
    }
    printf("=========================================\n");
    if (counter != 5) {
        printf("Something went wrong! Should've been selected 5 items, but got: %d", counter);
        close_iterator(iter, db);
        db_close(db);
        return 1;
    }

    close_iterator(iter, db);

    off_t size1, size2, size3;

    // Measuring size of file before deletion
    size1 = lseek(get_fd(db), 0, SEEK_END);

    // Try deleting a table

    q = query_init_blank();
    q->type = DROP_TABLE;
    q->db = db;
    q->first_table_name = "Passports";

    res = execute_query(q, NULL);
    if (res != OK) {
        printf("In execute_query()\n");
        printf("%s", result_strings[res]);
        close_query(q);
        db_close(db);
        return 1;
    }

    close_query(q);

    // Now if we try to select from it, it should result in an error

    q = query_init_blank();
    q->type = SELECT_ALL;
    q->db = db;
    q->first_table_name = "Passports";
    iter = iter_init();

    res = execute_query(q, iter);
    if (res != NO_TABLE_FOUND) {
        printf("In execute_query()\n");
        printf("%s", result_strings[res]);
        free(iter);
        close_query(q);
        db_close(db);
        return 1;
    }
    // If it works correctly, you'll see an error message in an output:
    // "Error opening table: no such table found!"

    close_query(q);

    // Measuring size of file after deletion
    size2 = lseek(get_fd(db), 0, SEEK_END);

    printf("\n%ld, %ld", size1, size2);

    // Now let's create a table again to see if we reuse the memory:
    q = query_init_blank();
    q->type = CREATE_TABLE;
    q->db = db;
    q->first_table_name = "Passports";

    dt = init_data(2);

    res = init_str_col_data(dt, 0, "Name", "Vladislav");
    if (res != OK) {
        printf("In init_str_col_data()\n");
        printf("%s", result_strings[res]);
        close_query(q);
        close_data(dt);
        db_close(db);
        return 1;
    }
    res = init_str_col_data(dt, 1, "Passport ID", "36 16 420322");
    if (res != OK) {
        printf("In init_str_col_data()\n");
        printf("%s", result_strings[res]);
        close_query(q);
        close_data(dt);
        db_close(db);
        return 1;
    }

    q->dt = dt;


    res = execute_query(q, NULL);
    if (res != OK) {
        printf("In execute_query()\n");
        printf("%s", result_strings[res]);
        close_query(q);
        close_data(dt);
        db_close(db);
        return 1;
    }

    close_query(q);

    q = query_init_blank();
    q->type = INSERT;
    q->db = db;
    q->dt = dt;
    q->first_table_name = "Passports";

    res = execute_query(q, NULL);
    if (res != OK) {
        printf("In execute_query()\n");
        printf("%s", result_strings[res]);
        close_query(q);
        close_data(dt);
        db_close(db);
        return 1;
    }

    close_query(q);
    close_data(dt);

    // We've put fewer data in it, so we can see that the file won't shrink:
    size3 = lseek(get_fd(db), 0, SEEK_END);

    printf("\n%ld, %ld, %ld\n", size1, size2, size3);

    free(iter);


    // Now let's test performance:
//    test_large_data_performance(db);
    test_delete(db);
    test_join(db);

    db_close(db);
    return 0;
}
