#include "database.h"

void close_iterator(row_iterator* iterator, database* db) {
    if (iterator == NULL) return;
    if (iterator->page != NULL) {
        tableHeaderPage* table = iterator->page->table;
        data_page_write(db->file_descriptor, iterator->page);
        close_data_page(iterator->page);
        if (table->is_virtual) {
            remove_table(db, table);
        }
    }
    free(iterator);
}

void init_db(database* db, int fd, mainHeader* header, tableHeaderPage* last_used_table, freePage* freeP) {
    db->file_descriptor = fd;
    db->header = header;
    db->last_used_table = last_used_table;
    db->first_free_page = freeP;
}

enum result open_table(database* db, tableHeaderPage** table, const char * table_name) {
    if ((*table) != NULL && str_equals(table_name, (*table)->table_name)) return OK;
    tableHeader* header;
    char * name;
    ssize_t bytes_read;
    uint32_t next = db->header->first_table_offset;
    uint32_t found_offset = 0;
    bool found = false;
    enum result res;
    while (!found) {
        if (next == 0) break;
        header = malloc(sizeof(tableHeader));
        res = read_table_header(header, db->file_descriptor, next);
        if (res != OK) {
            printf("In read_table_header()");
            free(header);
            return res;
        }
        name = malloc(header->table_name_len);
        bytes_read = read(db->file_descriptor, name, header->table_name_len);
        if (bytes_read == -1) {
            printf("Error reading file: %d\n", errno);
            free(header);
            free(name);
            return ERROR_READING_FILE;
        }
        found = str_equals(table_name, name);
        if (found) found_offset = header->this_offset;
        next = header->next_table_offset;
        free(header);
        free(name);
    }
    if (!found) {
        printf("Error opening table: no such table found!");
        return NO_TABLE_FOUND;
    }
    res = write_table(db->file_descriptor, (*table));
    if (res != OK) {
        printf("In write_table()");
        return res;
    }
    close_table((*table));
    (*table) = malloc(sizeof(tableHeaderPage));
    res = read_table((*table), db->file_descriptor, found_offset);
    if (res != OK) {
        printf("In read_table()");
        free((*table));
        return res;
    }
    return OK;
}


enum result read_free_page(freePage* p, int fd, uint32_t off) {
    off_t offset = lseek(fd, off, SEEK_SET);

    if (offset == -1) {
        printf("Error seeking: %d\n", errno);
        return ERROR_SEEKING;
    }

    p->header = malloc(sizeof(freeHeader));

    ssize_t bytes_read = read(fd, p->header, sizeof(freeHeader));
    if (bytes_read == -1) {
        printf("Error reading file: %d\n", errno);
        free(p->header);
        return ERROR_READING_FILE;
    }
    p->next_page = NULL;
    return OK;
}

enum result write_free_page(int fd, freePage* freeP) {
    off_t offset = lseek(fd, freeP->header->this_offset, SEEK_SET);

    if (offset == -1) {
        printf("Error seeking: %d\n", errno);
        return ERROR_SEEKING;
    }
    ssize_t bytes_written = write(fd, freeP->header, sizeof(freeHeader));
    if (bytes_written == -1) {
        printf("Error writing to file: %d\n", errno);
        return ERROR_WRITING_TO_FILE;
    }
    return OK;
}

void close_free_page(freePage* freeP) {
    if (freeP == NULL) return;
    free(freeP->header);
    free(freeP);
}

freePage* free_page_init(mainHeader* header, uint32_t off) {
    freePage* freeP = malloc(sizeof(freePage));
    freeP->header = malloc(sizeof(freeHeader));
    freeP->header->this_offset = off;
    freeP->header->next_free_page_offset = 0;
    freeP->header->page_size_kilobytes = header->page_size_kilobytes;
    freeP->next_page = NULL;
    return freeP;
}

enum result db_open(database* db, const char* filename, uint32_t page_size_kilobytes, uint32_t max_string_length) {
    int fd = open(filename,O_RDWR, S_IWUSR | S_IRUSR);
    bool existed = true;
    if (fd == -1) {
        existed = false;
        fd = open(filename,O_RDWR | O_CREAT, S_IWUSR | S_IRUSR);
    }
    if (fd == -1) {
        printf("Unable to open file\n");
        return UNABLE_TO_OPEN_FILE;
    }
    mainHeader* header;
    tableHeaderPage* firstTPage;
    freePage* freeP;
    enum result res;
    if (existed) {
        header = malloc(sizeof(mainHeader));
        res = read_header(header, fd);
        if (res != OK) {
            printf("In read_header()");
            free(header);
            return res;
        }
        freeP = malloc(sizeof(freePage));
        res = read_free_page(freeP, fd, header->first_free_page_offset);
        if (res != OK) {
            printf("In read_free_page()");
            free(freeP);
            return res;
        }
        freePage* start = freeP;
        freePage* old = start;
        while (start->header->next_free_page_offset != 0) {
            start = malloc(sizeof(freePage));
            res = read_free_page(start, fd, old->header->next_free_page_offset);
            if (res != OK) {
                printf("In read_free_page()");
                free(start);
                return res;
            }
            old->next_page = start;
            old = start;
        }
    } else {
        header = init_header(page_size_kilobytes, max_string_length);
        freeP = free_page_init(header, header->first_free_page_offset);
    }
    firstTPage = NULL;

    init_db(db, fd, header, firstTPage, freeP);
    db->join_used_table = NULL;
    return OK;
}

enum result db_close(database* db) {
    if (db == NULL) return OK;
    enum result res = write_header(db->file_descriptor, db->header);
    if (res != OK) {
        printf("In write_header()");
        return res;
    }
    res = write_table(db->file_descriptor, db->last_used_table);
    if (res != OK) {
        printf("In write_table()");
        return res;
    }
    freePage* fp = db->first_free_page;
    while (fp->next_page != NULL) {
        res = write_free_page(db->file_descriptor, fp);
        if (res != OK) {
            printf("In write_free_page()");
            return res;
        }
        fp = fp->next_page;
    }
    res = write_free_page(db->file_descriptor, fp);
    if (res != OK) {
        printf("In write_free_page()");
        return res;
    }

    close_header(db->header);
    close_table(db->last_used_table);
    close_table(db->join_used_table);
    fp = db->first_free_page;
    freePage* next = fp->next_page;
    while (next != NULL) {
        close_free_page(fp);
        fp = next;
        next = next->next_page;
    }
    close_free_page(fp);
    free(db);
    return OK;
}

tableHeaderPage* new_basic_table(database* db, uint32_t columns_num) {
    tableHeaderPage* page = malloc(sizeof(tableHeaderPage));
    page->header = malloc(sizeof(tableHeader));
    page->header->max_string_length = db->header->max_string_length;
    page->header->columns_num = columns_num;
    page->header->page_size_kilobytes = db->header->page_size_kilobytes;
    page->header->this_offset = db->header->first_free_page_offset;
    page->header->first_data_page_offset = db->header->first_free_page_offset;
    page->table_name = NULL;
    page->columns = malloc(sizeof(column) * page->header->columns_num);
    page->header->num_pages = 0;
    page->header->next_table_offset = 0;
    page->header->initialized = false;
    page->file_descriptor = db->file_descriptor;
    page->is_virtual = true;

    return page;
}

enum result new_table(database* db, const char * table_name, uint32_t columns_num) {
    tableHeaderPage* page = new_basic_table(db, columns_num);
    page->is_virtual = false;
    int len = 1;
    while (table_name[len - 1] != '\0') len++;
    page->header->table_name_len = len;
    page->table_name = malloc(page->header->table_name_len);
    for (int i = 0; i < len; i++) {
        page->table_name[i] = table_name[i];
    }
    page->table_name[len - 1] = '\0';

    page->header->this_offset = db->first_free_page->header->this_offset;
    freePage* old = db->first_free_page;
    db->first_free_page = db->first_free_page->next_page;
    close_free_page(old);
    if (db->first_free_page != NULL) {
        db->header->first_free_page_offset = db->first_free_page->header->this_offset;
    } else {
        db->header->first_free_page_offset += db->header->page_size_kilobytes * 1024;

        freePage* freeP = free_page_init(db->header, db->header->first_free_page_offset);
        db->first_free_page = freeP;
    }

    // Find last table and add pointer to newly created one
    tableHeader* header;
    enum result res;
    if (db->header->n_tables > 0) {
        if (db->last_used_table != NULL) {
            header = db->last_used_table->header;
        } else {
            header = malloc(sizeof(tableHeader));
            res = read_table_header(header, db->file_descriptor, db->header->first_table_offset);
            if (res != OK) {
                printf("In read_table_header()");
                free(header);
                return res;
            }
        }
        while (header->next_table_offset != 0) {
            off_t offst = header->next_table_offset;
            if (db->last_used_table == NULL || header != db->last_used_table->header) free(header);
            header = malloc(sizeof(tableHeader));
            res = read_table_header(header, db->file_descriptor, offst);
            if (res != OK) {
                printf("In read_table_header()");
                free(header);
                return res;
            }
        }
        header->next_table_offset = page->header->this_offset;
        res = write_table_header(db->file_descriptor, header);
        if (res != OK) {
            printf("In write_table_header()");
            return res;
        }
        if (db->last_used_table == NULL || header != db->last_used_table->header) free(header);
    }

    db->header->n_tables++;
    write_table(db->file_descriptor, page);
    close_table(page);
    return OK;
}

enum result remove_table(database* db, tableHeaderPage* table) {
    dataPage* dp = malloc(sizeof(dataPage));
    enum result res;
    res = read_data(dp, table->file_descriptor, table->header->first_data_page_offset, table);
    if (res != OK) {
        printf("In read_data()\n");
        return res;
    }
    if (dp != NULL) {
        dataPage* old = dp;
        while (old->header->next_page_offset != 0) {
            dp = malloc(sizeof(dataPage));
            res = read_data(dp, db->file_descriptor, old->header->next_page_offset, table);
            if (res != OK) {
                printf("In read_data()\n");
                return res;
            }
            freePage* freeP = free_page_init(db->header, old->header->this_offset);
            freeP->header->next_free_page_offset = db->first_free_page->header->this_offset;
            freeP->next_page = db->first_free_page;
            db->first_free_page = freeP;
            close_data_page(old);
            old = dp;
        }
        freePage* freeP = free_page_init(db->header, dp->header->this_offset);
        freeP->header->next_free_page_offset = db->first_free_page->header->this_offset;
        freeP->next_page = db->first_free_page;
        db->first_free_page = freeP;
        close_data_page(dp);
    }

    tableHeader* prev = malloc(sizeof(tableHeader));
    res = read_table_header(prev, db->file_descriptor, db->header->first_table_offset);
    if (res != OK) {
        printf("In read_table_header()");
        free(prev);
        return res;
    }
    if (prev->this_offset == table->header->this_offset) {
        db->header->first_table_offset = table->header->next_table_offset;
        res = write_header(db->file_descriptor, db->header);
        if (res != OK) {
            printf("In write_header()");
            free(prev);
            return res;
        }
    } else {
        while (prev->next_table_offset != table->header->this_offset) {
            uint32_t off = prev->next_table_offset;
            free(prev);
            prev = malloc(sizeof(tableHeader));
            res = read_table_header(prev, db->file_descriptor, off);
            if (res != OK) {
                printf("Int read_table_header()");
                free(prev);
                return res;
            }
        }
        prev->next_table_offset = table->header->next_table_offset;
        res = write_table_header(db->file_descriptor, prev);
        if (res != OK) {
            printf("In write_table_header()");
            free(prev);
            return res;
        }
        if (db->join_used_table != NULL && prev->this_offset == db->join_used_table->header->this_offset) {
            db->join_used_table->header->next_table_offset = table->header->next_table_offset;
        }
        if (db->last_used_table != NULL && prev->this_offset == db->last_used_table->header->this_offset) {
            db->last_used_table->header->next_table_offset = table->header->next_table_offset;
        }
    }

    if (db->last_used_table == table) db->last_used_table = NULL;

    freePage* freeP = free_page_init(db->header, table->header->this_offset);
    freeP->header->next_free_page_offset = db->first_free_page->header->this_offset;
    freeP->next_page = db->first_free_page;
    db->first_free_page = freeP;
    db->header->first_free_page_offset = freeP->header->this_offset;
    db->header->n_tables--;
    close_table(table);
    free(prev);
    return OK;
}

enum result insert_data(database* db, tableHeaderPage* table, data* dt) {
    if (table->header->columns_num != dt->col_num) {
        printf("Data does not fit the table\n");
        return DATA_DOESNT_FIT;
    }
    uint32_t sum_size = 0;
    for (int i = 0; i < dt->col_num; i++) {
        sum_size += table->columns[i].header->col_size;
        if (dt->columns[i].header->col_header != table->columns[i].header->col_header) {
            printf("Data does not fit the table\n");
            return DATA_DOESNT_FIT;
        }
    }
    // Check if there are no pages allocated to Table
    // or if the last page is full
    dataPage *page = malloc(sizeof(dataPage));
    if (table->header->num_pages == 0) {

        data_page_init(table, db->first_free_page->header->this_offset);
        if (table->table_name != NULL) {
            freePage* fp = db->first_free_page;
            db->first_free_page = db->first_free_page->next_page;
            close_free_page(fp);
            if (db->first_free_page != NULL) {
                db->header->first_free_page_offset = db->first_free_page->header->this_offset;
            } else {
                db->header->first_free_page_offset += db->header->page_size_kilobytes * 1024;

                freePage* freeP = free_page_init(db->header, db->header->first_free_page_offset);
                db->first_free_page = freeP;
            }
        }

    } else {
        read_data(page, table->file_descriptor, table->header->last_data_page_offset, table);
        // Check if last table is full, and we need a new one
        if (page->header->free_spc_offset + sum_size - page->header->this_offset > db->header->page_size_kilobytes * 1024) {

            data_page_init(table, db->first_free_page->header->this_offset);
            if (table->table_name != NULL) {
                freePage* fp = db->first_free_page;
                db->first_free_page = db->first_free_page->next_page;
                close_free_page(fp);
                if (db->first_free_page != NULL) {
                    db->header->first_free_page_offset = db->first_free_page->header->this_offset;
                } else {
                    db->header->first_free_page_offset += db->header->page_size_kilobytes * 1024;

                    freePage* freeP = free_page_init(db->header, db->header->first_free_page_offset);
                    db->first_free_page = freeP;
                }
            }

        }
        close_data_page(page);
        page = malloc(sizeof(dataPage));
    }

    read_data(page, table->file_descriptor, table->header->last_data_page_offset, table);
    // page - current page where we want to write
    // Writing bytes from query to Table at offset

    for (int i = 0; i < dt->col_num; i++) {
        switch (dt->columns[i].header->col_header) {
            case BOOLEAN:
                ((bool*) page->data[page->header->rows_num][i])[0] = ((bool*) dt->data[i])[0];
                break;
            case INT32:
                ((int32_t*) page->data[page->header->rows_num][i])[0] = ((int32_t*) dt->data[i])[0];
                break;
            case DOUBLE:
                ((double*) page->data[page->header->rows_num][i])[0] = ((double*) dt->data[i])[0];
                break;
            case VARCHAR:
                for (int j = 0; j < dt->columns[i].header->col_size; j++) {
                    ((char*) (page->data[page->header->rows_num][i]))[j] = ((char*) dt->data[i])[j];
                }
                break;
            default:
                return UNKNOWN_COLUMN_TYPE;
        }

    }

    page->header->rows_num++;
    page->header->free_spc_offset += sum_size;
    data_page_write(table->file_descriptor, page);
    close_data_page(page);
    return OK;
}

enum result update_data_where(database* db, tableHeaderPage* table, data* dt, const char * col_name, enum columnType type, void* value) {
    if (table->header->columns_num != dt->col_num) {
        printf("Data does not fit the table\n");
        return DATA_DOESNT_FIT;
    }
    bool col_exists = false;
    uint32_t col_n = 0;
    for (int i = 0; i < dt->col_num; i++) {
        if (dt->columns[i].header->col_header != table->columns[i].header->col_header) {
            printf("Data does not fit the table\n");
            return DATA_DOESNT_FIT;
        }
        if (str_equals(col_name, table->columns[i].name) &&
            table->columns[i].header->col_header == type) {
            col_n = i;
            col_exists = true;
            break;
        }
    }
    if (!col_exists) {
        printf("Error updating - no such column found");
        return DATA_DOESNT_FIT;
    }
    row_iterator* iterator = malloc(sizeof(row_iterator));
    table_begin(table, iterator);
    while (iterator->row_n != iterator->page->header->rows_num) {
        bool this = false;
        switch (type) {
            case INT32:
                if (*((int32_t *)iterator->page->data[iterator->row_n][col_n]) == *((int32_t *)value)) this = true;
                break;
            case VARCHAR:
                if (str_equals((char *)iterator->page->data[iterator->row_n][col_n], (char *) value)) this = true;
                break;
            case DOUBLE:
                if (*((double *)iterator->page->data[iterator->row_n][col_n]) == *((double *)value)) this = true;
                break;
            case BOOLEAN:
                if (*((bool *)iterator->page->data[iterator->row_n][col_n]) == *((bool *)value)) this = true;
                break;
            default:
                printf("Error updating - invalid column type provided!");
                return UNKNOWN_COLUMN_TYPE;
        }
        if (this) {
            for (int i = 0; i < dt->col_num; i++) {
                switch (dt->columns[i].header->col_header) {
                    case BOOLEAN:
                        ((bool*) iterator->page->data[iterator->row_n][i])[0] = ((bool*) dt->data[i])[0];
                        break;
                    case INT32:
                        ((int32_t*) iterator->page->data[iterator->row_n][i])[0] = ((int32_t*) dt->data[i])[0];
                        break;
                    case DOUBLE:
                        ((double*) iterator->page->data[iterator->row_n][i])[0] = ((double*) dt->data[i])[0];
                        break;
                    case VARCHAR:
                        for (int j = 0; j < dt->columns[i].header->col_size; j++) {
                            ((char*) (iterator->page->data[iterator->row_n][i]))[j] = ((char*) dt->data[i])[j];
                        }
                        break;
                    default:
                        printf("Error updating - invalid column type provided!");
                        return UNKNOWN_COLUMN_TYPE;
                }
            }
        }
        enum result res = table_next(iterator);
        if (res != OK) {
            printf("In table_next()");
            close_iterator(iterator, db);
            return res;
        }
    }
    close_iterator(iterator, db);
    return OK;
}

enum result remove_data_page(database* db, dataPage* dp) {
    enum result res;
    tableHeaderPage* table = dp->table;
    dataPage* prev = malloc(sizeof(dataPage));
    if (dp->header->this_offset != table->header->first_data_page_offset) {
        res = read_data(prev, table->file_descriptor, dp->header->prev_page_offset, table);
        if (res != OK) {
            printf("In read_data()\n");
            return res;
        }
    } else {
        free(prev);
        prev = dp;
    }
    freePage* new_free_page = free_page_init(db->header, dp->header->this_offset);
    db->header->first_free_page_offset = new_free_page->header->this_offset;
    new_free_page->next_page = db->first_free_page;
    db->first_free_page = new_free_page;
    if (dp != prev) {
        prev->header->next_page_offset = 0;
        table->header->last_data_page_offset = prev->header->this_offset;
        res = data_page_write(table->file_descriptor, prev);
        if (res != OK) {
            printf("In data_page_write()\n");
            return res;
        }
        close_data_page(prev);
    }
    close_data_page(dp);
    table->header->num_pages--;
    if (table->header->num_pages == 0) {
        table->header->first_data_page_offset = 0;
        table->header->last_data_page_offset = 0;
    }
    return OK;
}

enum result remove_where(database* db, tableHeaderPage* table, const char * col_name, enum columnType type, void* value) {
    bool col_exists = false;
    uint32_t col_n = 0;
    for (int i = 0; i < table->header->columns_num; i++) {
        if (str_equals(col_name, table->columns[i].name) &&
            table->columns[i].header->col_header == type) {
            col_n = i;
            col_exists = true;
            break;
        }
    }
    if (!col_exists) {
        printf("Error removing - no such column found");
        return DATA_DOESNT_FIT;
    }
    row_iterator* iterator = malloc(sizeof(row_iterator));
    table_begin(table, iterator);
    row_iterator* back_iterator = malloc(sizeof(row_iterator));
    table_end(table, back_iterator);
    enum result res = table_prev(back_iterator);
    if (res != OK) {
        printf("In table_prev()");
        close_iterator(iterator, db);
        close_iterator(back_iterator, db);
        return res;
    }

    while (table->header->num_pages != 0 && iterator->page != NULL && back_iterator->page != NULL &&
    (iterator->page->header->page_num < back_iterator->page->header->page_num ||
    (iterator->page->header->page_num == back_iterator->page->header->page_num && iterator->row_n <= back_iterator->row_n))) {
        bool this = false;
        if (iterator->page->header->this_offset == back_iterator->page->header->this_offset &&
            iterator->page != back_iterator->page) {
            void*** swap = iterator->page->data;
            iterator->page->data = back_iterator->page->data;
            back_iterator->page->data = swap;
            close_data_page(iterator->page);
            iterator->page = back_iterator->page;
        }
        switch (type) {
            case INT32:
                if (*((int32_t *)iterator->page->data[iterator->row_n][col_n]) == *((int32_t *)value)) this = true;
                break;
            case VARCHAR:
                if (str_equals((char *)iterator->page->data[iterator->row_n][col_n], (char *) value)) this = true;
                break;
            case DOUBLE:
                if (*((double *)iterator->page->data[iterator->row_n][col_n]) == *((double *)value)) this = true;
                break;
            case BOOLEAN:
                if (*((bool *)iterator->page->data[iterator->row_n][col_n]) == *((bool *)value)) this = true;
                break;
            default:
                printf("Error removing - invalid column type provided!");
                return UNKNOWN_COLUMN_TYPE;
        }
        if (this) {


            for (int i = 0; i < table->header->columns_num; i++) {
                void * swapper = iterator->page->data[iterator->row_n][i];
                iterator->page->data[iterator->row_n][i] = back_iterator->page->data[back_iterator->row_n][i];
                back_iterator->page->data[back_iterator->row_n][i] = swapper;
            }

            dataPage* dp;
            if (iterator->page == back_iterator->page && iterator->row_n == 0 && iterator->row_n == back_iterator->row_n) {
                dp = iterator->page;
                res = remove_data_page(db, dp);
                if (res != OK) {
                    printf("In remove_data_page()\n");
                    return res;
                }
                iterator->page = NULL;
                back_iterator->page = NULL;
                continue;
            }

            uint32_t off = back_iterator->page->header->this_offset;
            res = table_prev(back_iterator);
            if (res != OK) {
                printf("In table_prev()");
                close_iterator(iterator, db);
                close_iterator(back_iterator, db);
                return res;
            }

            if (back_iterator->page->header->this_offset == off) {
                back_iterator->page->header->rows_num--;
            } else {
                dp = malloc(sizeof(dataPage));
                res = read_data(dp, back_iterator->page->table->file_descriptor, off, back_iterator->page->table);
                if (res != OK) {
                    printf("In read_data()\n");
                    return res;
                }
                dp->header->rows_num--;
                remove_data_page(db, dp);
                back_iterator->page->header->next_page_offset = 0;
            }
        } else {
            if (iterator->page == back_iterator->page && iterator->row_n == iterator->page->header->rows_num - 1) {
                back_iterator->page = NULL;
            }
            res = table_next(iterator);
            if (res != OK) {
                printf("In table_next()");
                close_iterator(iterator, db);
                close_iterator(back_iterator, db);
                return res;
            }
        }
    }

    if (back_iterator->page == iterator->page) iterator->page = NULL;
    close_iterator(back_iterator, db);
    close_iterator(iterator, db);
    return OK;
}

enum result select_all(row_iterator* iter, database* db, tableHeaderPage* table) {
    char* new_name = str_concat(table->table_name, "_SELECT_ALL");
    enum result res;
    res = new_table(db, new_name, table->header->columns_num);
    if (res != OK) {
        printf("In new_table()\n");
        free(new_name);
        return res;
    }
    tableHeaderPage* virt_table = NULL;
    res = open_table(db, &virt_table, new_name);
    if (res != OK) {
        printf("In open_join_table()\n");
        free(new_name);
        return res;
    }
    free(new_name);

    for (int i = 0; i < virt_table->header->columns_num; i++) {
        switch (table->columns[i].header->col_header) {
            case INT32:
                init_int32_col_table(virt_table, i, table->columns[i].name);
                break;
            case VARCHAR:
                init_str_col_table(virt_table, i, table->columns[i].name);
                break;
            case DOUBLE:
                init_double_col_table(virt_table, i, table->columns[i].name);
                break;
            case BOOLEAN:
                init_bool_col_table(virt_table, i, table->columns[i].name);
                break;
            default:
                return UNKNOWN_COLUMN_TYPE;
        }
    }
    virt_table->header->initialized = true;

    row_iterator* iterator = malloc(sizeof(row_iterator));
    table_begin(table, iterator);
    while (iterator->page != NULL && iterator->row_n != iterator->page->header->rows_num) {
        data* dt = extract_iterator(iterator);
        insert_data(db, virt_table, dt);
        close_data(dt);
        res = table_next(iterator);
        if (res != OK) {
            printf("In table_next()");
            close_iterator(iterator, db);
            return res;
        }
    }
    close_iterator(iterator, db);
    table_begin(virt_table, iter);
    virt_table->is_virtual = true;
    return OK;
}

enum result select_where(row_iterator* iter, database* db, tableHeaderPage* table, const char * col_name, enum columnType type, void* value) {
    bool col_exists = false;
    uint32_t col_n = 0;
    for (int i = 0; i < table->header->columns_num; i++) {
        if (str_equals(col_name, table->columns[i].name) &&
            table->columns[i].header->col_header == type) {
            col_n = i;
            col_exists = true;
            break;
        }
    }
    if (!col_exists) {
        printf("Error selecting - no such column found");
        return DATA_DOESNT_FIT;
    }
    char* first = str_concat(table->table_name, "_SELECT_WHERE_");
    char* second = str_concat(first, col_name);
    free(first);
    char* third = str_concat(second, "=");
    free(second);
    char* val;
    char* new_name;
    switch (type) {
        case INT32:
            val = malloc(sizeof(int32_t) + 1);
            sprintf(val, "%d", *((int32_t *) value));
            new_name = str_concat(third, val);
            free(val);
            break;
        case VARCHAR:
            new_name = str_concat(third, (char *) value);
            break;
        case DOUBLE:
            val = malloc(sizeof(double) + 1);
            sprintf(val, "%f", *((double *) value));
            new_name = str_concat(third, val);
            free(val);
            break;
        case BOOLEAN:
            val = malloc(sizeof(bool) + 1);
            sprintf(val, "%d", *((bool *) value));
            new_name = str_concat(third, val);
            free(val);
            break;
        default:
            return UNKNOWN_COLUMN_TYPE;
    }

    free(third);

    enum result res;
    res = new_table(db, new_name, table->header->columns_num);
    if (res != OK) {
        printf("In new_table()\n");
        free(new_name);
        return res;
    }
    tableHeaderPage* virt_table = NULL;
    res = open_table(db, &virt_table, new_name);
    if (res != OK) {
        printf("In open_join_table()\n");
        free(new_name);
        return res;
    }
    free(new_name);
    for (int i = 0; i < virt_table->header->columns_num; i++) {
        switch (table->columns[i].header->col_header) {
            case INT32:
                init_int32_col_table(virt_table, i, table->columns[i].name);
                break;
            case VARCHAR:
                init_str_col_table(virt_table, i, table->columns[i].name);
                break;
            case DOUBLE:
                init_double_col_table(virt_table, i, table->columns[i].name);
                break;
            case BOOLEAN:
                init_bool_col_table(virt_table, i, table->columns[i].name);
                break;
            default:
                return UNKNOWN_COLUMN_TYPE;
        }
    }
    virt_table->header->initialized = true;
    row_iterator* iterator = malloc(sizeof(row_iterator));
    table_begin(table, iterator);
    while (iterator->row_n != iterator->page->header->rows_num) {
        data* dt = extract_iterator(iterator);
        switch (type) {
            case INT32:
                if (*((int32_t *)dt->data[col_n]) == *((int32_t *)value)) insert_data(db, virt_table, dt);
                break;
            case VARCHAR:
                if (str_equals(dt->data[col_n],(char *) value)) insert_data(db, virt_table, dt);
                break;
            case DOUBLE:
                if (*((double *)dt->data[col_n]) == *((double *)value)) insert_data(db, virt_table, dt);
                break;
            case BOOLEAN:
                if (*((bool *)dt->data[col_n]) == *((bool *)value)) insert_data(db, virt_table, dt);
                break;
            default:
                printf("Error selecting - invalid column type provided!");
                return DATA_DOESNT_FIT;
        }
        close_data(dt);
        res = table_next(iterator);
        if (res != OK) {
            printf("In table_next()");
            close_iterator(iterator, db);
            return res;
        }
    }
    close_iterator(iterator, db);
    table_begin(virt_table, iter);
    virt_table->is_virtual = true;
    return OK;
}

enum result join_tables(row_iterator* iter, database* db, tableHeaderPage* table1, const char * col_name_t1, tableHeaderPage* table2, const char * col_name_t2) {
    bool col1_exists = false;
    uint32_t col1_n = 0;
    for (int i = 0; i < table1->header->columns_num; i++) {
        if (str_equals(col_name_t1, table1->columns[i].name)) {
            col1_n = i;
            col1_exists = true;
            break;
        }
    }
    bool col2_exists = false;
    uint32_t col2_n = 0;
    for (int i = 0; i < table2->header->columns_num; i++) {
        if (str_equals(col_name_t2, table2->columns[i].name)) {
            col2_n = i;
            col2_exists = true;
            break;
        }
    }

    if (!col1_exists || !col2_exists) {
        printf("Error joining - no such column found");
        return DATA_DOESNT_FIT;
    }
    if (table1->columns[col1_n].header->col_header != table2->columns[col2_n].header->col_header) {
        printf("Error joining - different types of columns");
        return DATA_DOESNT_FIT;
    }
    char* new_name = str_concat(table1->table_name, table2->table_name);

    enum result res;
    res = new_table(db, new_name, table1->header->columns_num + table2->header->columns_num - 1);
    if (res != OK) {
        printf("In new_table()\n");
        free(new_name);
        return res;
    }
    tableHeaderPage* virt_table = NULL;
    res = open_table(db, &virt_table, new_name);
    if (res != OK) {
        printf("In open_join_table()\n");
        free(new_name);
        return res;
    }
    free(new_name);
    for (int i = 0; i < table1->header->columns_num; i++) {
        switch (table1->columns[i].header->col_header) {
            case INT32:
                init_int32_col_table(virt_table, i, table1->columns[i].name);
                break;
            case VARCHAR:
                init_str_col_table(virt_table, i, table1->columns[i].name);
                break;
            case DOUBLE:
                init_double_col_table(virt_table, i, table1->columns[i].name);
                break;
            case BOOLEAN:
                init_bool_col_table(virt_table, i, table1->columns[i].name);
                break;
            default:
                return UNKNOWN_COLUMN_TYPE;
        }
    }
    uint32_t counter = table1->header->columns_num;
    for (int i = 0; i < table2->header->columns_num; i++) {
        if (i == col2_n) {
            counter -= 1;
            continue;
        }
        switch (table2->columns[i].header->col_header) {
            case INT32:
                init_int32_col_table(virt_table, counter + i, table2->columns[i].name);
                break;
            case VARCHAR:
                init_str_col_table(virt_table, counter + i, table2->columns[i].name);
                break;
            case DOUBLE:
                init_double_col_table(virt_table, counter + i, table2->columns[i].name);
                break;
            case BOOLEAN:
                init_bool_col_table(virt_table, counter + i, table2->columns[i].name);
                break;
            default:
                return UNKNOWN_COLUMN_TYPE;
        }
    }
    virt_table->header->initialized = true;
    row_iterator* iterator1 = malloc(sizeof(row_iterator));
    table_begin(table1, iterator1);
    row_iterator* iterator2 = malloc(sizeof(row_iterator));
    table_begin(table2, iterator2);
    data* dt;
    while (iterator1->row_n != iterator1->page->header->rows_num) {
        while (iterator2->row_n != iterator2->page->header->rows_num) {
            bool this = false;
            switch (table1->columns[col1_n].header->col_header) {
                case INT32:
                    if (*((int32_t*)iterator1->page->data[iterator1->row_n][col1_n]) == *((int32_t*)iterator2->page->data[iterator2->row_n][col2_n])) this = true;
                    break;
                case VARCHAR:
                    if (str_equals((char *)iterator1->page->data[iterator1->row_n][col1_n], (char *)iterator2->page->data[iterator2->row_n][col2_n])) this = true;
                    break;
                case DOUBLE:
                    if (*((double *)iterator1->page->data[iterator1->row_n][col1_n]) == *((double *)iterator2->page->data[iterator1->row_n][col2_n])) this = true;
                    break;
                case BOOLEAN:
                    if (*((bool *)iterator1->page->data[iterator1->row_n][col1_n]) == *((bool *)iterator2->page->data[iterator2->row_n][col2_n])) this = true;
                    break;
                default:
                    return UNKNOWN_COLUMN_TYPE;
            }
            if (this) {
                dt = init_data(virt_table->header->columns_num);
                for (int i = 0; i < table1->header->columns_num; i++) {
                    switch (table1->columns[i].header->col_header) {
                        case INT32:
                            init_int32_col_data(dt, i, table1->columns[i].name, *((int32_t*)iterator1->page->data[iterator1->row_n][i]));
                            break;
                        case VARCHAR:
                            init_str_col_data(dt, i, table1->columns[i].name, (char*)iterator1->page->data[iterator1->row_n][i]);
                            break;
                        case DOUBLE:
                            init_double_col_data(dt, i, table1->columns[i].name, *((double*)iterator1->page->data[iterator1->row_n][i]));
                            break;
                        case BOOLEAN:
                            init_bool_col_data(dt, i, table1->columns[i].name, *((bool*)iterator1->page->data[iterator1->row_n][i]));
                            break;
                        default:
                            return UNKNOWN_COLUMN_TYPE;
                    }
                }
                counter = table1->header->columns_num;
                for (int i = 0; i < table2->header->columns_num; i++) {
                    if (i == col2_n) {
                        counter -= 1;
                        continue;
                    }
                    switch (table2->columns[i].header->col_header) {
                        case INT32:
                            init_int32_col_data(dt, counter + i, table2->columns[i].name, *((int32_t*)iterator2->page->data[iterator2->row_n][i]));
                            break;
                        case VARCHAR:
                            init_str_col_data(dt, counter + i, table2->columns[i].name, (char*)iterator2->page->data[iterator2->row_n][i]);
                            break;
                        case DOUBLE:
                            init_double_col_data(dt, counter + i, table2->columns[i].name, *((double*)iterator2->page->data[iterator2->row_n][i]));
                            break;
                        case BOOLEAN:
                            init_bool_col_data(dt, counter + i, table2->columns[i].name, *((bool*)iterator2->page->data[iterator2->row_n][i]));
                            break;
                        default:
                            return UNKNOWN_COLUMN_TYPE;
                    }
                }
                res = insert_data(db, virt_table, dt);
                if (res != OK) {
                    printf("In insert_data()");
                    close_iterator(iterator1, db);
                    close_iterator(iterator2, db);
                    return res;
                }
                close_data(dt);
            }
            res = table_next(iterator2);
            if (res != OK) {
                printf("In table_next()");
                close_iterator(iterator1, db);
                close_iterator(iterator2, db);
                return res;
            }
        }
        close_iterator(iterator2, db);
        iterator2 = malloc(sizeof(row_iterator));
        table_begin(table2, iterator2);
        res = table_next(iterator1);
        if (res != OK) {
            printf("In table_next()");
            close_iterator(iterator1, db);
            close_iterator(iterator2, db);
            return res;
        }
    }
    close_iterator(iterator1, db);
    close_iterator(iterator2, db);
    table_begin(virt_table, iter);
    virt_table->is_virtual = true;
    return OK;
}
