#ifndef RELATIONALDATABASE_DATABASE_H
#define RELATIONALDATABASE_DATABASE_H

#include "mainHeader.h"

typedef struct freeHeader freeHeader;
typedef struct freePage freePage;
typedef struct database database;


struct database{
    int file_descriptor;
    mainHeader* header;
    tableHeaderPage* last_used_table;
    tableHeaderPage* join_used_table;
    freePage* first_free_page;
};

void init_db(database* db, int fd, mainHeader* header, tableHeaderPage* last_used_table, freePage* freeP);

tableHeaderPage* new_basic_table(database* db, uint32_t columns_num);

struct freeHeader {
    uint32_t this_offset;
    uint32_t next_free_page_offset;
    uint32_t page_size_kilobytes;
};

struct freePage {
    freeHeader* header;
    freePage* next_page;
};

enum result open_table(database* db, tableHeaderPage** table, const char * table_name);
//enum result open_join_table(database* db, const char * table_name);


enum result read_free_page(freePage* p, int fd, uint32_t off);
enum result write_free_page(int fd, freePage* freeP);

void close_free_page(freePage* freeP);

freePage* free_page_init(mainHeader* header, uint32_t off);

void close_iterator(row_iterator* iterator, database* db);

// Queries
enum result new_table(database* db, const char * table_name, uint32_t columns_num);
enum result remove_table(database* db, tableHeaderPage* table);

enum result insert_data(database* db, tableHeaderPage* table, data* dt);
enum result update_data_where(database* db, tableHeaderPage* table, data* dt, const char * col_name, enum columnType type, void* value);
enum result remove_where(database* db, tableHeaderPage* table, const char * col_name, enum columnType type, void* value);

enum result select_all(row_iterator* iter, database* db, tableHeaderPage* table);
enum result select_where(row_iterator* iter, database* db, tableHeaderPage* table, const char * col_name, enum columnType type, void* value);

enum result join_tables(row_iterator* iter, database* db, tableHeaderPage* table1, const char * col_name_t1, tableHeaderPage* table2, const char * col_name_t2);



#endif //RELATIONALDATABASE_DATABASE_H
