#include "mainHeader.h"

mainHeader* init_header(uint32_t page_size_kilobytes, uint32_t max_string_length) {
    mainHeader* header = malloc(sizeof(mainHeader));
    uint32_t actual_page_size = 1;
    while (actual_page_size < page_size_kilobytes) actual_page_size *= 2;

    header->page_size_kilobytes = actual_page_size;
    header->n_tables = 0;
    header->first_table_offset = sizeof(mainHeader);
    header->first_free_page_offset = header->first_table_offset;
    header->max_string_length = max_string_length;
    return header;
}

enum result read_header(mainHeader* header, int fd) {
    ssize_t bytes_read = read(fd, header, sizeof(mainHeader));
    if (bytes_read == -1) {
        printf("Error reading file: %d\n", errno);
        return ERROR_READING_FILE;
    }
    return OK;
}

void close_header(mainHeader* header) {
    if (header == NULL) return;
    free(header);
}

enum result write_header(int fd, mainHeader* header) {
    off_t offset = lseek(fd, 0, SEEK_SET);

    if (offset == -1) {
        printf("Error seeking: %d\n", errno);
        return ERROR_SEEKING;
    }

    ssize_t bytes_written = write(fd, header, sizeof(mainHeader));
    if (bytes_written == -1) {
        printf("Error writing to file: %d\n", errno);
        return ERROR_WRITING_TO_FILE;
    }
    return OK;
}



