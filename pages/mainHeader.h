#ifndef RELATIONALDATABASE_MAINHEADER_H
#define RELATIONALDATABASE_MAINHEADER_H

#include "table.h"

typedef struct mainHeader mainHeader;
struct mainHeader {
    uint32_t n_tables;
    uint32_t page_size_kilobytes;
    uint32_t first_table_offset;
    uint32_t first_free_page_offset;
    int32_t max_string_length;
};

mainHeader* init_header(uint32_t page_size_kilobytes, uint32_t max_string_length);

enum result read_header(mainHeader* header, int fd);

void close_header(mainHeader* header);

enum result write_header(int fd, mainHeader* header);

#endif //RELATIONALDATABASE_MAINHEADER_H
