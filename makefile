debug:
	clang *.c pages/*.c -Wall -Werror -g -o ./test_program_debug
sanitized:
	clang *.c pages/*.c -Wall -Werror -g -fsanitize=address,undefined,leak -o ./test_program_sanitized
release:
	clang *.c pages/*.c -Wall -Werror -g -O3 -o ./test_program_release
windows:
	x86_64-w64-mingw32-gcc -o ./test_program.exe *.c pages/*.c
