#ifndef RELATIONALDATABASE_QUERY_H
#define RELATIONALDATABASE_QUERY_H

#include "data.h"

enum queryType {
    CREATE_TABLE,
    DROP_TABLE,
    INSERT,
    UPDATE,
    DELETE,
    SELECT_ALL,
    SELECT,
    JOIN
};

typedef struct query query;
typedef struct row_iterator row_iterator;
typedef struct database database;
typedef struct tableHeaderPage tableHeaderPage;


struct query {
    enum queryType type;
    database * db;
    char * first_table_name;
    char * second_table_name;
    data * dt;
    char * col_name_first;
    enum columnType col_type;
    void* value;
    char * col_name_second;
};

query* query_init_blank();

void close_query(query* q);

enum result execute_query(query* q, row_iterator* iterator);

database* db_init();
enum result db_open(database* db, const char* filename, uint32_t page_size_kilobytes, uint32_t max_string_length);
enum result db_close(database* db);

row_iterator* iter_init();
void table_begin(tableHeaderPage* table, row_iterator* iter);
void table_end(tableHeaderPage* table, row_iterator* iter);
enum result table_next(row_iterator* iterator);
enum result table_prev(row_iterator* iterator);
data* extract_iterator(row_iterator* iterator);
void close_iterator(row_iterator* iterator, database* db);
bool iter_valid(row_iterator* iterator);

int get_fd(database* db);


#endif //RELATIONALDATABASE_QUERY_H
