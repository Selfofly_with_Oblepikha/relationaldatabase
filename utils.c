#include "utils.h"

bool str_equals(const char * str1, const char * str2) {
    int i = 0;
    while (str1[i] == str2[i]) {
        if (str1[i] == '\0') return true;
        i++;
    }
    return false;
}

char * str_concat(const char * str1, const char * str2) {
    int first_len = 0, second_len = 0;
    while (str1[first_len] != '\0') {
        first_len++;
    }
    while (str2[second_len] != '\0') {
        second_len++;
    }
    char * new_str = malloc(sizeof(char) * (first_len + second_len + 1));
    for (int i = 0; i < first_len; i++) {
        new_str[i] = str1[i];
    }
    for (int i = first_len; i < first_len + second_len; i++) {
        new_str[i] = str2[i - first_len];
    }
    new_str[first_len + second_len] = '\0';
    return new_str;
}