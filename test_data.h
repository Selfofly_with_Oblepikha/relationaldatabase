#ifndef RELATIONALDATABASE_TEST_DATA_H
#define RELATIONALDATABASE_TEST_DATA_H

#include "query.h"

void insert_first_test_data(database* db);

void insert_second_test_data(database* db);

void test_large_data_performance(database* db);

void test_delete(database* db);

void test_join(database* db);



#endif //RELATIONALDATABASE_TEST_DATA_H
