#include "data.h"

data* init_data(uint32_t col_n) {

    data* dt = malloc(sizeof(data));
    dt->col_num = col_n;
    dt->columns = malloc(sizeof(column) * dt->col_num);
    dt->data = malloc(dt->col_num * sizeof(void*));
    for (int i = 0; i < dt->col_num; i++) {
        dt->columns[i].header = NULL;
        dt->columns[i].name = NULL;
        dt->data[i] = NULL;
    }
    return dt;
}

void close_data(data* data) {
    if (data == NULL) return;
    for (int i = 0; i < data->col_num; i++) {
        if (data->columns[i].header != NULL) free(data->columns[i].header);
        if (data->data[i] != NULL) free(data->data[i]);
        if (data->columns[i].name != NULL) free(data->columns[i].name);
    }
    free(data->columns);
    free(data->data);
    free(data);
}

enum result init_basic_col_data(data* data, uint32_t col_n, const char * col_name) {
    if (col_n > data->col_num) {
        printf("Error addressing column: not enough columns");
        return ERROR_ADDRESSING_COLUMN;
    }
    data->columns[col_n].header = malloc(sizeof(columnHeader));
    int len = 1;
    while (col_name[len - 1] != '\0') len++;
    data->columns[col_n].header->col_name_len = len;
    data->columns[col_n].name = malloc(len);
    for (int i = 0; i < len; i++) {
        data->columns[col_n].name[i] = col_name[i];
    }
    data->columns[col_n].name[len - 1] = '\0';
    return OK;
}

enum result init_int32_col_data(data* data, uint32_t col_n, const char * col_name, int32_t value) {
    enum result res = init_basic_col_data(data, col_n, col_name);
    if (res != OK) {
        printf("In init_basic_col_data()");
        return res;
    }
    data->columns[col_n].header->col_header = INT32;
    data->columns[col_n].header->col_size = sizeof(int32_t);

    data->data[col_n] = malloc(data->columns[col_n].header->col_size);
    ((int32_t *) data->data[col_n])[0] = value;
    return OK;
}

enum result init_double_col_data(data* data, uint32_t col_n, const char * col_name, double value) {
    enum result res = init_basic_col_data(data, col_n, col_name);
    if (res != OK) {
        printf("In init_basic_col_data()");
        return res;
    }
    data->columns[col_n].header->col_header = DOUBLE;
    data->columns[col_n].header->col_size = sizeof(double);

    data->data[col_n] = malloc(data->columns[col_n].header->col_size);
    ((double *) data->data[col_n])[0] = value;
    return OK;
}

enum result init_bool_col_data(data* data, uint32_t col_n, const char * col_name, bool value) {
    enum result res = init_basic_col_data(data, col_n, col_name);
    if (res != OK) {
        printf("In init_basic_col_data()");
        return res;
    }
    data->columns[col_n].header->col_header = BOOLEAN;
    data->columns[col_n].header->col_size = sizeof(bool);

    data->data[col_n] = malloc(data->columns[col_n].header->col_size);
    ((bool *) data->data[col_n])[0] = value;
    return OK;
}

enum result init_str_col_data(data* data, uint32_t col_n, const char * col_name, const char * value) {
    enum result res = init_basic_col_data(data, col_n, col_name);
    if (res != OK) {
        printf("In init_basic_col_data()");
        return res;
    }
    data->columns[col_n].header->col_header = VARCHAR;
    int len = 1;
    while (value[len - 1] != '\0') len++;
    uint32_t col_size = len;
    data->columns[col_n].header->col_size = col_size;
    data->data[col_n] = malloc(col_size);
    for (int i = 0; i < len; i++) {
        ((char *) data->data[col_n])[i] = value[i];
    }
    ((char *) data->data[col_n])[col_size - 1] = '\0';
    return OK;
}