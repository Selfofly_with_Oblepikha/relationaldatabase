#ifndef RELATIONALDATABASE_UTILS_H
#define RELATIONALDATABASE_UTILS_H


#include <malloc.h>
#include <unistd.h>
#include <stdbool.h>
#include <inttypes.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/stat.h>

bool str_equals(const char * str1, const char * str2);
char * str_concat(const char * str1, const char * str2);

enum result {
    OK,
    UNKNOWN_QUERY_TYPE,
    UNKNOWN_COLUMN_TYPE,
    NOT_ENOUGH_ARGUMENTS,
    ERROR_READING_FILE,
    NO_TABLE_FOUND,
    ERROR_SEEKING,
    UNABLE_TO_OPEN_FILE,
    ERROR_WRITING_TO_FILE,
    DATA_DOESNT_FIT,
    NO_SUCH_ELEMENT,
    ITERATOR_INVALID,
    ERROR_ADDRESSING_COLUMN
};

static const char* result_strings[] = {
        "OK",
        "UNKNOWN_QUERY_TYPE",
        "UNKNOWN_COLUMN_TYPE",
        "NOT_ENOUGH_ARGUMENTS",
        "ERROR_READING_FILE",
        "NO_TABLE_FOUND",
        "ERROR_SEEKING",
        "UNABLE_TO_OPEN_FILE",
        "ERROR_WRITING_TO_FILE",
        "DATA_DOESNT_FIT",
        "NO_SUCH_ELEMENT",
        "ITERATOR_INVALID",
        "ERROR_ADDRESSING_COLUMN"
};




#endif //RELATIONALDATABASE_UTILS_H
